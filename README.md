# Code Generator Base Plugin

This is a base plugin for a
[code generator](https://gitlab.com/AdeAttwood/code-gen) plugin.

## Usage

To use the base app you can clone this repo into your `~/.code-g` directory
and then install it in your `index.js`.

~~~ sh
git clone https://gitlab.com/AdeAttwood/code-gen-base-plugin my-plugin
~~~

~~~ js
// ~/.code-g/index.js

const MyPlugin = require('./my-plugin');

module.exports = {
    ...

    plugins: [
        ...

        MyPlugin,

        ...
    ],

    ...
};

~~~