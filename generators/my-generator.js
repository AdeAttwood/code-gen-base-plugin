'use strict';

const CodeGen = require('code-gen');

class MyGenerator {

    constructor(programme) {
        let command = programme.command('namespace:my-generator');

        command.action(this.action);
        command.description('Test generator');
        command.option('-n, --file-name <value>', 'The name of the file');

        this.command = command;
    }

    static generate(fileName) {
        let generator = new CodeGen.Generator({
            'templateName': 'my-template',
            'name': fileName,
            'basePath': process.cwd(),
            'fileExtension': 'txt',
            'args': {
                'myKey': 'My value',
            }
        });

        generator.templatePaths.push(__dirname + '/../templates');

        return generator.save();;
    }

    action() {
        if (!this.fileName) {
            CodeGen.Output.error('Option file-name is required');
            process.exit(1);
        }

        MyGenerator.generate(this.fileName);
    }

}

module.exports = MyGenerator;
