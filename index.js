'use strict';

const MyGenerator = require('./generators/my-generator');

class Generator {

    register(programme) {
        new MyGenerator(programme);
    }

};

module.exports = new Generator;
