/* global it, describe, beforeEach, afterEach */
'use strict';

/**
 * Test the generator creates a file
 *
 * @author  Ade Attwood <code@adeattwood.co.uk>
 * @since   @next_version@
 */

const assert = require('code-g/test/assert');
const Command = require('commander').Command;
const Generator = require('../generators/my-generator');
const Fs = require('fs');

describe('my-generator', function() {

    beforeEach(() => {
        this.app = new Command('Test Command');
        new Generator(this.app);
    });

    afterEach(() => {
        Fs.unlinkSync('my-file.txt');
    });

    it('Should create a file', () => {
        this.app.commands[0].fileName = 'my-file';
        this.app.emit('command:namespace:my-generator');

        assert.fileExists('my-file.txt');
    });

});
