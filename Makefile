PWD=$(shell pwd)
NODE=${PWD}/node_modules/.bin

CMD=

all: lint test

install:
	npm install;

lint:
	${NODE}/eslint src test;
	${NODE}/validate-npm-package;

test:
	${NODE}/mocha --recursive ${CMD};

coverage:
	${NODE}/nyc ${NODE}/mocha --recursive;
	${NODE}/nyc report --reporter=html;

clean:
	rm -rf ${PWD}/.nyc_output ${PWD}/coverage
